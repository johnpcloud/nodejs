module.exports = {
    routeRequest : (req,res) => {
        // res.statusCode = 200;
        // res.setHeader('Content-Type','text/plain');

        res.writeHead(200,{'Content-Type':'text/plain'});
    
        var url = req.url;
        if(url ==='/'){
            res.end('Home page');
        }else if(url === '/about'){
            res.end('About Page');
        }else{
            res.writeHead(404);
            res.end('Route Not definded');
        }
    }
};