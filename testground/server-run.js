const http = require('http');
const route = require('./route');
const port = 3000;

http.createServer(route.routeRequest).listen(port);